use std::error::Error;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
struct Record {
    name: String,
    age: u32,
    city: String,
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut rdr = csv::Reader::from_path("data.csv")?;

    for result in rdr.deserialize() {
        let record: Record = result?;
        println!("{:?}", record);
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_data_ingestion() {
        let records = read_data("data.csv").unwrap();
        assert_eq!(records.len(), 3); 
        assert_eq!(records[0].name, "John Doe");
        assert_eq!(records[0].age, 30);
        assert_eq!(records[0].city, "New York");
    }


    fn read_data(file_path: &str) -> Result<Vec<Record>, Box<dyn Error>> {
        let mut rdr = csv::Reader::from_path(file_path)?;
        let mut records = Vec::new();
        for result in rdr.deserialize() {
            let record: Record = result?;
            records.push(record);
        }
        Ok(records)
    } 
}