
# Rust CLI Tool for CSV Data Processing

This Rust command-line tool demonstrates simple data ingestion and processing from a CSV file. It reads records from a `.csv` file, processes them, and prints the output. The project also includes basic unit tests to ensure the functionality works as expected.


## Usage

The tool expects a CSV file named `data.csv` in the root of the project directory with the following structure:

```csv
name,age,city
John Doe,30,New York
Jane Smith,25,Los Angeles
Mike Brown,45,Chicago
```

To run the tool, use Cargo from the command line:

```bash
cargo run
```

This will compile and execute the application, processing the `data.csv` file and printing the records to the console.

## Functionality

The application demonstrates basic CSV data ingestion using the Rust `csv` crate and `serde` for deserialization. It reads a specified CSV file, deserializes each record into a Rust struct, and prints it.

## Testing

The project includes a simple unit test to ensure data ingestion and processing works as expected. To run the tests, execute the following command:

```bash
cargo test
```

This will run all the tests included in the project and output the results.

## Result
![image](1470e5364513c5cf3dc9c2d321ebb2a.png)
![image](7905b3333ab15b49239713ad9d5827b.png)
